let inputValue1, inputValue2, inputValue3, inputValue4;
let input_1, input_2, input_3, input_4;

const change = () => {
    setInterval(() => {
        input_1 = $('#input_1');
        input_2 = $('#input_2');
        input_3 = $('#input_3');
        input_4 = $('#input_4');

        inputValue1 = $('#input_1').val();
        inputValue2 = $('#input_2').val();
        inputValue3 = $('#input_3').val();
        inputValue4 = $('#input_4').val();
    }, 1);

    const colorChange = (input1, input2, input3, input4, event, inputV1, inputV2, inputV3, inputV4) => {
        $(input1).css('background-color', event.target.value)
        if (inputV1 == inputV2) {
            $(input2).css('background-color', event.target.value)
        }
        if (inputV1 == inputV3) {
            $(input3).css('background-color', event.target.value)
        }
        if (inputV1 == inputV4) {
            $(input4).css('background-color', event.target.value)
        }
        
    }

    document.querySelector('#first-input').addEventListener('click', event => {
        colorChange(input_1, input_2, input_3, input_4, event, inputValue1, inputValue2, inputValue3, inputValue4);
    });

    document.querySelector('#second-input').addEventListener('click', event => {
        colorChange(input_2, input_1, input_3, input_4, event, inputValue2, inputValue1, inputValue3, inputValue4);
    });

    document.querySelector('#third-input').addEventListener('click', event => {
        colorChange(input_3, input_2, input_1, input_4, event, inputValue3, inputValue2, inputValue1, inputValue4);
    });

    document.querySelector('#forth-input').addEventListener('click', event => {
        colorChange(input_4, input_2, input_3, input_1, event, inputValue4, inputValue2, inputValue3, inputValue1);
    });
}

change();

